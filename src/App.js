import React from "react";
import { ToDoList } from "./components/todo/ToDoList";

export const App = () => {
  const todos = [
    { id: 1, content: "buy milk" },
    { id: 2, content: "play wow" },
    { id: 3, content: "contemplate life" }
  ];

  return <div className="App"><ToDoList todos={todos}></ToDoList></div>;
};
