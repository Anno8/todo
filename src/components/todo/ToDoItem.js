import React, { useState } from "react";

export const ToDoItem = ({ todo, deleteToDo }) => {
  const [done, setDone ] = useState(false);

  return (
    <div className="collection-item" onClick={() => setDone(!done)}>
      <span className={`${done && 'strikeout'}`}>{todo.content}</span>
      <i className="material-icons right" onClick={deleteToDo}>delete</i>
    </div>
  );
};
