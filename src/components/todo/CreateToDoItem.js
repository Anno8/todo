import React, { useState } from "react";
export const CreateToDoItem = ({ create }) => {
  const [content, setContent] = useState("");

  return (
    <form onSubmit={e => {
        create(e, { content });
        setContent("");
      }}
    >
      <label className="black-text">Add new todo item:</label>
      <input
        pattern=".{3,}" 
        required
        title="3 characters minimum"
        type="text"
        className="input-field"
        value={content}
        onChange={e => setContent(e.target.value)}
      />
    </form>
  );
};
