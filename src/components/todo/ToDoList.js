import React, { useState } from "react";
import { ToDoItem } from "./ToDoItem";
import { CreateToDoItem } from "./CreateToDoItem";

export const ToDoList = ({ todos }) => {
  const [list, setTodos] = useState(todos);

  const todoList = list.length ? 
  <div className="collection">
    {list.map(todo => 
      <ToDoItem
        key={todo.id}
        todo={todo}
        deleteToDo={() => deleteToDo(todo.id)}/>
    )}</div> : 
    <div className="collection-item">
      <span>You have nothing to do.</span>
    </div>;

  const deleteToDo = id => setTodos(list.filter(todo => todo.id !== id));
  const createToDo = (e, todo) => {
      e.preventDefault();
      todo.id = list.length > 0 ? Math.max(...list.map(todo => todo.id)) + 1 : 1;
      setTodos([...list, todo]);
  }
  

  return (
    <div className="container">
      <h1 className="center">Todos</h1>
      {todoList}
      <CreateToDoItem create={createToDo} />
    </div>
  );
};
